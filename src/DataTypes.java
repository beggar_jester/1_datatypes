public class DataTypes {
    public static void main(String[] args) {
        int CreditBookNumber = 0x1e219;
        long PhoneNumber = 8_921_123_45_67L;
        int TwoLastDigits = 0b1000011;
        int FourLastDigits = 010727;
        int StudentJournalNumber = 29;
        int ProcessedStudentNumber = ((StudentJournalNumber - 1) % 26) + 1;
        char EnglishLetter = (char) (ProcessedStudentNumber + 64);

        System.out.println(CreditBookNumber);
        System.out.println(PhoneNumber);
        System.out.println(TwoLastDigits);
        System.out.println(FourLastDigits);
        System.out.println(ProcessedStudentNumber);
        System.out.println(EnglishLetter);
    }
}
